FROM adoptopenjdk/openjdk11:alpine-jre

WORKDIR /app

COPY target/vendas-0.0.1-SNAPSHOT.jar /app/spring-app.jar

COPY data /app/data

ENTRYPOINT ["java", "-jar", "spring-app.jar"] 