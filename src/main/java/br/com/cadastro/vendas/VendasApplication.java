package br.com.cadastro.vendas;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.cadastro.vendas.model.RetornoArquivoEntrada;
import br.com.cadastro.vendas.model.util.EntradaArquivo;
import br.com.cadastro.vendas.model.util.SaidaArquivo;

@SpringBootApplication
public class VendasApplication {
	
	public static void main(String[] args) {
		Logger logger = Logger.getLogger(VendasApplication.class);
		SpringApplication.run(VendasApplication.class, args);
		
		logger.info("Sistema iniciado em: " + new Date());
		
		File arquivos[];
		// Caminho do arquivo de entrada
		File diretorio = new File("data//in");
		
		// Caminho do arquivo de saida
		String pathArquivoSaida = "data//out//exemploDadosSaida.done.dat";

		arquivos = diretorio.listFiles();
		List<String> listaArquivos = new ArrayList<String>();

		try {
			for (int i = 0; i < arquivos.length; i++) {
				String extensaoDoArquivo = FilenameUtils.getExtension(arquivos[i].toString());
				if (extensaoDoArquivo.equals("dat")) {
					listaArquivos.add(arquivos[i].toString());
				}
			}
			
			RetornoArquivoEntrada listaRetorno = new RetornoArquivoEntrada();
			for (String arquivo : listaArquivos) {
				EntradaArquivo.arquivoEntrada(arquivo, listaRetorno);
			}
			SaidaArquivo.gerarArquivoSaida(pathArquivoSaida, listaRetorno);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
	}

}
