package br.com.cadastro.vendas.model;

import lombok.Data;

@Data
public class Cliente {

	private int idLinha;
	private Long cnpj;
	private String nome;
	private String areaDeAtuacao;
}
