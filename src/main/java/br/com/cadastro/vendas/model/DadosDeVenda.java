package br.com.cadastro.vendas.model;
import java.util.List;

import lombok.Data;

@Data
public class DadosDeVenda {

//	003çSale IDç[Item ID-Item Quantity-Item Price]çSalesman name
	private int idLinha;
	private int idVenda;
	private List<Item> itens;
	private String nomeVendedor;
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DadosDeVenda other = (DadosDeVenda) obj;
		if (idVenda != other.idVenda)
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idVenda;
		return result;
	}
	
	
}
