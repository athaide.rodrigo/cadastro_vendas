package br.com.cadastro.vendas.model;

import lombok.Data;

@Data
public class Item {
	
//	Item ID-Item Quantity-Item Price
	private int idItem;
	private int quantidade;
	private double preco;

}
