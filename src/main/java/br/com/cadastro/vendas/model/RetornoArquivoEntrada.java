package br.com.cadastro.vendas.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class RetornoArquivoEntrada {

	List<Vendedor> vendedores = new ArrayList<Vendedor>();
	List<Cliente> clientes = new ArrayList<Cliente>();
	List<DadosDeVenda> dadosDeVendas = new ArrayList<DadosDeVenda>();
}
