package br.com.cadastro.vendas.model;

import lombok.Data;

@Data
public class Vendedor {

	private int idLinha;
	private long cpf;
	private String nome;
	private double salario;
	private double valorEmVendas;
	
	
	
}
