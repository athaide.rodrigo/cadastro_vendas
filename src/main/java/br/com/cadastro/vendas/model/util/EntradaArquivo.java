package br.com.cadastro.vendas.model.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.cadastro.vendas.model.Cliente;
import br.com.cadastro.vendas.model.DadosDeVenda;
import br.com.cadastro.vendas.model.Item;
import br.com.cadastro.vendas.model.RetornoArquivoEntrada;
import br.com.cadastro.vendas.model.Vendedor;

public class EntradaArquivo {

	public static void arquivoEntrada(String path, RetornoArquivoEntrada listaRetornos) {

		Logger logger = Logger.getLogger(EntradaArquivo.class);
		
		List<String> linhas;

		try {
			linhas = ManipuladorArquivoLeitura.leitor(path);
			for (String linha : linhas) {
				if (linha != null) {

					String[] textoSeparado = linha.split("ç");

					if (textoSeparado[0].equals("001")) {
//						001çCPFçNameçSalary
						Vendedor vendedor = new Vendedor();
						vendedor.setIdLinha(Integer.parseInt(textoSeparado[0]));
						vendedor.setCpf(Long.parseLong(textoSeparado[1]));
						vendedor.setNome(textoSeparado[2]);
						vendedor.setSalario(Double.parseDouble(textoSeparado[3]));

						if (!listaRetornos.getVendedores().contains(vendedor)) {
							listaRetornos.getVendedores().add(vendedor);
						}

					}
					if (textoSeparado[0].equals("002")) {
//						002çCNPJçNameçBusiness Area
						Cliente cliente = new Cliente();
						cliente.setIdLinha(Integer.parseInt(textoSeparado[0]));
						cliente.setCnpj(Long.parseLong(textoSeparado[1]));
						cliente.setNome(textoSeparado[2]);
						cliente.setAreaDeAtuacao(textoSeparado[2]);
						if (!listaRetornos.getClientes().contains(cliente)) {
							listaRetornos.getClientes().add(cliente);
						}

					}
					if (textoSeparado[0].equals("003")) {
//						003çSale IDç[Item ID-Item Quantity-Item Price]çSalesman name
						DadosDeVenda dados = new DadosDeVenda();
						dados.setIdLinha(Integer.parseInt(textoSeparado[0]));
						dados.setIdVenda(Integer.parseInt(textoSeparado[1]));
						List<Item> listaItem = new ArrayList<Item>();
						String[] listaItens = textoSeparado[2].replace("[", "").replace("]", "").split(",");
						for (String item : listaItens) {
							if (item != null) {
								String[] itemSeparado = item.split("-");
								Item itemValue = new Item();
								itemValue.setIdItem(Integer.parseInt(itemSeparado[0]));
								itemValue.setQuantidade(Integer.parseInt(itemSeparado[1]));
								itemValue.setPreco(Double.parseDouble(itemSeparado[2]));

								listaItem.add(itemValue);
							}
						}
						dados.setItens(listaItem);
						dados.setNomeVendedor(textoSeparado[3]);

						if (!listaRetornos.getDadosDeVendas().contains(dados)) {
							listaRetornos.getDadosDeVendas().add(dados);
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}
	
}
