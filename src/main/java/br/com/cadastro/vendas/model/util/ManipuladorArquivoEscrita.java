package br.com.cadastro.vendas.model.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import org.apache.log4j.Logger;

public class ManipuladorArquivoEscrita {

	
	public static void escrever(String path, String saida) {
		Logger logger = Logger.getLogger(ManipuladorArquivoEscrita.class);
		File arquivo = new File(path);
		try {
			if (!arquivo.exists()) {
				// cria um arquivo (vazio)
				arquivo.createNewFile();
			}
			FileWriter fw = new FileWriter(arquivo, true);
			
			// escreve no arquivo
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(saida);
			bw.newLine();
			bw.close();
			fw.close();

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

}
