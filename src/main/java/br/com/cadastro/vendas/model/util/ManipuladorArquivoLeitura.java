package br.com.cadastro.vendas.model.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class ManipuladorArquivoLeitura {

	public static List<String> leitor(String path) {

		Logger logger = Logger.getLogger(ManipuladorArquivoLeitura.class);
		List<String> retorno = new ArrayList<String>();

		try {

			BufferedReader buffRead = new BufferedReader(new FileReader(path));
			String linha = "";
			while (true) {
				if (linha == null) {
					break;
				}
				linha = buffRead.readLine();
				retorno.add(linha);
			}
			buffRead.close();
		} catch (IOException e) {
			logger.error(e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return retorno;
	}

}
