package br.com.cadastro.vendas.model.util;

import org.apache.log4j.Logger;

import br.com.cadastro.vendas.model.DadosDeVenda;
import br.com.cadastro.vendas.model.Item;
import br.com.cadastro.vendas.model.RetornoArquivoEntrada;
import br.com.cadastro.vendas.model.Vendedor;

public class SaidaArquivo {

	public static void gerarArquivoSaida(String path, RetornoArquivoEntrada listaRetorno) {

		Logger logger = Logger.getLogger(SaidaArquivo.class);

		try {
			//Melhor vendedor
			int idVendaMaisCara = 0;
			double valorVendaMaisCara = 0;
			
			for (DadosDeVenda dadosDeVenda : listaRetorno.getDadosDeVendas()) {
				double valorTemp = 0;
				for (Item item : dadosDeVenda.getItens()) {
					valorTemp += item.getPreco();
				}
				
				for (Vendedor vendedor : listaRetorno.getVendedores()) {
					if(vendedor.getNome().equalsIgnoreCase(dadosDeVenda.getNomeVendedor())) {
						vendedor.setValorEmVendas(vendedor.getValorEmVendas() + valorTemp);
					}
				}
				
				if (valorTemp > valorVendaMaisCara) {
					idVendaMaisCara = dadosDeVenda.getIdVenda();
					valorVendaMaisCara = valorTemp;
				}
			}
			
			//Pior vendedor
			double piorValorEmVendas = 0;
			String nomePiorVendedor = null;
			for (Vendedor vendedor : listaRetorno.getVendedores()) {
				if(piorValorEmVendas == 0) {
					piorValorEmVendas = vendedor.getValorEmVendas(); 
					nomePiorVendedor = vendedor.getNome();
				} else if(vendedor.getValorEmVendas() < piorValorEmVendas) {
					piorValorEmVendas = vendedor.getValorEmVendas(); 
					nomePiorVendedor = vendedor.getNome();
				}
			}
			
			String saida = "=======================Arquivo de Saída======================== \n"
					 + "Quantidade de clientes: " + listaRetorno.getClientes().size() + "\n" 
					 + "Quantidade de vendedores: " + listaRetorno.getVendedores().size() + "\n" 
					 + "Id da venda mais cara: " + idVendaMaisCara + "\n"
					 + "Pior vendedor: " + nomePiorVendedor;
			
			ManipuladorArquivoEscrita.escrever(path, saida);
		
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
	}
	
}
